
# XFCE Win-OS2 theme

Xfwm4 theme to resemble Windows 3.1, with the Win-OS2 color scheme.

![xfce4-terminal decorated to look like Win-OS2](images/xfce4-winos2.png)

Inspired by [Progman](https://github.com/jcs/progman), a Win31 look-alike
window manager.

Looks good with the Mint-Y-Teal GTK theme.  Originally bootstrapped from the
[Mint-Y-Teal](https://github.com/linuxmint/mint-themes) theme, (c) 2012-2014
Clement Lefebvre.

Available from: [Xfce-look project page](https://www.xfce-look.org/p/2132039/)

License: GPL-3+ (see COPYING.txt).
